#!/bin/bash

# ==========================
# Lijantropique Inc.
# Project: Kali Linux ISO recipe
# lijantropique@protonmail.com
# Desktop 	    : kali-desktop-xfce
# Metapackages	: kali-linux-light 
# ISO size 	    : ~1.7 GB 
# Look and Feel	: Custom wallpaper and terminal configs through post install hooks.
# References    : 
#       g0tmi1k         https://gitlab.com/kalilinux/recipes/live-build-config-examples/blob/master/kali-linux-mate-top10-nonroot.sh 
#       metapackages    https://tools.kali.org/kali-metapackages
# ==========================

# 1- Update and install dependencies
apt-get update --fix-missing
apt-get -y upgrade
apt-get install git live-build cdebootstrap devscripts -y

# 2 - Clone the default Kali live-build config.
git clone https://gitlab.com/kalilinux/build-scripts/live-build-config.git

# 3 - Customizations
cd live-build-config

# 3.1 The packages that would be avilable after installation
# Additional kali packages can be found here: https://tools.kali.org/kali-metapackages
cat > kali-config/variant-light/package-lists/kali.list.chroot << EOF
# Kali applications
apache2
arping
arp-scan
burpsuite
cadaver
cewl
exploitdb
fierce
git
gobuster
hydra
hydra-gtk
impacket-scripts
john
masscan
netdiscover
nikto
nmap
onesixtyone
passing-the-hash
php
php-mysql
proxychains
python-impacket
pure-ftpd
recon-ng
seclists
smbmap
snmpcheck
sqlitebrowser
sqlmap
webshells
wfuzz
whatweb
windows-binaries
winexe
wireshark
wordlists
wpscan

# Kali-variant
kali-linux-light

# Graphical desktop
kali-desktop-xfce
EOF

# 3.2 Additional debian packages 
cat > kali-config/common/package-lists/debian.list.chroot << EOF
hexchat
python3-pip
net-tools
xfce4-screenshooter
html2text
EOF

# 3.3 Additional packages
mkdir temp
cd temp
wget https://github.com/Tib3rius/AutoRecon/archive/master.zip -O AutoRecon.zip
wget https://github.com/FortyNorthSecurity/EyeWitness/archive/master.zip -O EyeWitness.zip
unzip EyeWitness.zip
unzip AutoRecon.zip
mv EyeWitness-*/ EyeWitness/
mv AutoRecon-*/ AutoRecon/
rm -rf *.zip

cd .. # back to live-build folder
mkdir -p kali-config/common/includes.chroot/opt
mv temp/* kali-config/common/includes.chroot/opt/

mkdir kali-config/common/packages.chroot
cp ../debPkg/*.deb kali-config/common/packages.chroot/

# 3.4 Unattended installation
cat > kali-config/common/hooks/20-unattended-boot.binary.chroot << EOF
#!/bin/bash
cat >>binary/isolinux/install.cfg < label install
    menu label ^Unattended Install
    menu default
    linux /install/vmlinuz
    initrd /install/initrd.gz
    append vga=788 — quiet file=/cdrom/install/preseed.cfg locale=en_US keymap=us hostname=kali domain=local.lan
END
EOF

chmod +x kali-config/common/hooks/20-unattended-boot.binary.chroot

# preseed
wget https://www.kali.org/dojo/preseed.cfg  -O ./kali-config/common/includes.installer/preseed.cfg

# 3.5 Custom wallpaper/splash screen
mkdir -p kali-config/common/includes.chroot/usr/share/desktop-base/active-theme/wallpaper/contents/images

cp ../skull.png kali-config/common/includes.chroot/usr/share/desktop-base/active-theme/wallpaper/contents/images/default.png
echo 'cp /usr/share/desktop-base/active-theme/wallpaper/contents/images/default.png /etc/alternatives/desktop-background' >>  kali-config/common/hooks/live/wallpaper.chroot
chmod +x kali-config/common/hooks/live/wallpaper.chroot

cp ../skull.png kali-config/common/includes.chroot/usr/share/desktop-base/active-theme/wallpaper/contents/images/desktop-grub.png
echo 'cp /usr/share/desktop-base/active-theme/wallpaper/contents/images/desktop-grub.png /etc/alternatives/desktop-grub' >>  kali-config/common/hooks/live/splash.chroot
chmod +x kali-config/common/hooks/live/splash.chroot

cp ../skull.svg kali-config/common/includes.chroot/usr/share/desktop-base/active-theme/wallpaper/contents/images/login-background.svg
echo 'cp /usr/share/desktop-base/active-theme/wallpaper/contents/images/login-background.svg /etc/alternatives/desktop-login-background' >>  kali-config/common/hooks/live/login.chroot
chmod +x kali-config/common/hooks/live/login.chroot

# 4. Run the build
./build.sh --variant light --distribution kali-rolling

